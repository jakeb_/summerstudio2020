# Seefood (Hotdog not hotdog app)
The repository contains code that was originally intended to be run only on the Raspberry Pi - using Rasbpian 9.
The following repository structure is indicated below:
```
```bash
seefood
| README.md
| docker-compose.yaml
├── pi_port
├── pi_server
| run.sh
├── training
```

## Pre-requisite software (Tested)
- Docker Desktop 19.03.6
- docker-compose 1.24.1
- qemu-user-static

## Usage
In order to run the app, you can run the below command. This will instantiate docker-compose to run the command
```bash
bash run.sh
```

## Docker Images used
You will need the following docker images:
- benthepleb/summerstudio2020:latest (Manually edited locally - did not upload to DockerHub - Dockerfile can be found in ```pi_server/Dockerfile```)
- pi_port (Dockerfile can be found within ```pi_port/Dockerfile```)
- pi_flow (didn't use this image, but Dockerfile be found within ```pi_server/Dockerfile.old```)

### What is qemu-user-static
This library allows you to build and emulate code/apps for different set of hardware - but using the x86 architecture (your computer hardware). Since the raspberry pi uses ARM - it requires a different set of packages in order for it run. Not all packages are available on ARM.

## Overall pipeline of the code
- ```training``` - all the code that was used to train the model and generate the tf_lite model is contained here.
- ```pi_server``` - the code that was required to run inference within a Python Flask Server
- ```pi_port``` - all the code that was required to use ngrok (which forwards your port to a public IP) and generate a qr code based on that address. If you refer to the code - there was some code which allowed you to email the qr code - but changed it to a figure plot of the qr code instead. Please refer to the ```pi_port/README.md``` for futher instructions to building the Docker Image.

## Why use docker-compose vs docker run?
Docker-compose allows you to launch multiple containers and connect them together. It also provides the capability to restart containers automatically - which is ideal for deployment since it was a live-running demo.
